In [1]: import numpy as np                                                                                                                                                                                  

In [2]: table = np.genfromtxt('/home/ignat/Документы/кр_экономика.csv', delimiter=';')                                                                                                                      

In [3]: short = table[:4]                                                                                                                                                                                   

In [4]: short                                                                                                                                                                                               
Out[4]: 
array([[ 0.,  0.,  0.,  0.],
       [ 1., 10., 10., 10.],
       [ 2., 26., 13., 16.],
       [ 3., 66., 22., 40.]])

In [5]: one = np.identity(4, dtype=float)                                                                                                                                                                   

In [6]: one                                                                                                                                                                                                 
Out[6]: 
array([[1., 0., 0., 0.],
       [0., 1., 0., 0.],
       [0., 0., 1., 0.],
       [0., 0., 0., 1.]])

In [7]: mult = short.dot(one)                                                                                                                                                                               

In [8]: mult                                                                                                                                                                                                
Out[8]: 
array([[ 0.,  0.,  0.,  0.],
       [ 1., 10., 10., 10.],
       [ 2., 26., 13., 16.],
       [ 3., 66., 22., 40.]])

In [9]: np.savetxt('/home/ignat/Документы/Matrix.csv', mult)
